﻿using UnityEngine;
using UnityEngine.UI;

public class HealZone : MonoBehaviour
{
    public PlayerHealth playerHealth;
    public Slider healthSlider;
    public int healthBoost = 20;

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            if(playerHealth.currentHealth < 100)
            {
                playerHealth.currentHealth += healthBoost;
            }
            //Debug.Log(playerHealth.currentHealth);
            healthSlider.value = playerHealth.currentHealth;
            Destroy(gameObject);
        }
    }
}
