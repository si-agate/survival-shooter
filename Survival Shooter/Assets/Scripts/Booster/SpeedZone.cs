﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpeedZone : MonoBehaviour
{
    public PlayerMovement playerMovement;
    public float moveBoost = 2f;
        
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            if (playerMovement.speed < 8f)
            {
                playerMovement.speed += moveBoost;
            }
            //Debug.Log(playerMovement.speed);
            Destroy(gameObject);
        }
    }
}
