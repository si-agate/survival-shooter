﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameOverManager : MonoBehaviour
{
    public Text warningText;
    public PlayerHealth playerHealth;       
    public float restartDelay = 5f;            
    
    Animator anim;                          
    float restartTimer;                    
    
    void Awake()
    {
        anim = GetComponent<Animator>();
    }


    void Update()
    {
        //Debug.Log("warning off");
        if (playerHealth.currentHealth <= 0)
        {
            anim.SetTrigger("GameOver");
            anim.ResetTrigger("Warning");
            restartTimer += Time.deltaTime;

            if (restartTimer >= restartDelay)
            {
                SceneManager.LoadScene(SceneManager.GetActiveScene().name);
            }
        }
    }

    public void ShowWarning(float enemyDistance)
    {
        enemyDistance = Mathf.RoundToInt(enemyDistance);
        //Debug.Log(enemyDistance);
        warningText.text = string.Format("! {0} m", enemyDistance);
        anim.SetTrigger("Warning");
        //Debug.Log("warning on");
    }
}